

#include<string>


class HtmlRequest {
private:
	HtmlRequest();
	~HtmlRequest();
	static std::string _result;
public:
	static void DoHtmlRequest(const std::string &url);
	static const std::string &GetResult();
};


